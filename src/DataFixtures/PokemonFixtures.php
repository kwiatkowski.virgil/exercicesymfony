<?php

namespace App\DataFixtures;

use App\Entity\Type;
use App\Entity\Pokemon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PokemonFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $types = $manager
        ->getRepository(Type::class)
        ->findAll();



        for ($i = 0; $i < 20; $i++) {
            $pokemon = new Pokemon();
            $pokemon->setName('Pickachu'.$i);
            $pokemon->setPP(rand($i,5000));
            $pokemon->setDefense(rand($i,5000));
            $pokemon->setAttack(rand($i,5000));
            $pokemon->setDescription('Ce pokemon est super fort !');
            // $pokemon->addType();
            $manager->persist($pokemon);
        }
        $manager->flush();
    }
}
