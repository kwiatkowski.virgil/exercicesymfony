<?php

namespace App\DataFixtures;

use App\Entity\Type;
use App\Controller\TypeController;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TypeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

            $type = new Type();
            $type->setName('Feu ');            
            $manager->persist($type);

            $type1 = new Type();
            $type1->setName('Eau ');            
            $manager->persist($type1);

            $type2 = new Type();
            $type2->setName('Electrique ');            
            $manager->persist($type2);

            $type3 = new Type();
            $type3->setName('Psy ');            
            $manager->persist($type3);

            $type4 = new Type();
            $type4->setName('Plante ');            
            $manager->persist($type4);

            $type5 = new Type();
            $type5->setName('Combat ');            
            $manager->persist($type5);

            $type6 = new Type();
            $type6->setName('Légendaire ');            
            $manager->persist($type6);
        
        $manager->flush();
    }
}
